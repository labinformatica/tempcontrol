#include "dns_reg.h"
#include "config_file.h"
#include "esp_http_client.h"
#include "esp_log.h"
#include <esp_crt_bundle.h>

esp_err_t _http_client_handler(esp_http_client_event_t *evt)
{
    switch (evt->event_id)
    {
    case HTTP_EVENT_ERROR:
        ESP_LOGD("DNS Update", "HTTP_EVENT_ERROR");
        break;
    case HTTP_EVENT_ON_CONNECTED:
        ESP_LOGD("DNS Update", "HTTP_EVENT_ON_CONNECTED");
        break;
    case HTTP_EVENT_HEADER_SENT:
        ESP_LOGD("DNS Update", "HTTP_EVENT_HEADER_SENT");
        break;
    case HTTP_EVENT_ON_HEADER:
        ESP_LOGD("DNS Update", "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
        break;
    case HTTP_EVENT_ON_DATA:
        ESP_LOGD("DNS Update", "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
        break;
    case HTTP_EVENT_ON_FINISH:
        ESP_LOGD("DNS Update", "HTTP_EVENT_ON_FINISH");
        break;
    case HTTP_EVENT_DISCONNECTED:
        ESP_LOGI("DNS Update", "HTTP_EVENT_DISCONNECTED");
        break;
    }
    return ESP_OK;
}

uint8_t xupdate_dns_reg(void)
{
    int retCode;
    esp_http_client_config_t config = {
        .event_handler = _http_client_handler,
        .url = get_config_data()->dns_update_uri,
        .cert_pem = get_config_data()->dns_cert,
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);
    /*
    esp_http_client_set_url(client, get_config_data()->dns_update_uri);
    esp_http_client_set_method(client, HTTP_METHOD_GET);
*/
    // GET
    esp_err_t err = esp_http_client_perform(client);
    retCode = 500;
    if (err == ESP_OK)
    {
        ESP_LOGI("DNS Update", "HTTP GET Status = %d, content_length = %d",
                 esp_http_client_get_status_code(client),
                 esp_http_client_get_content_length(client));
        retCode = esp_http_client_get_status_code(client);
    }
    else
    {
        ESP_LOGE("DNS Update", "HTTP GET request failed: %s", esp_err_to_name(err));
    }
    esp_http_client_cleanup(client);
    return (retCode == 200) ? 1 : 0;
}

esp_err_t client_handle(esp_http_client_event_t *evt)
{
    static int count = 0;
    switch (evt->event_id)
    {
    case HTTP_EVENT_ERROR:
        break;
    case HTTP_EVENT_ON_CONNECTED:
        count = 0;
        break;
    case HTTP_EVENT_HEADER_SENT:
        break;
    case HTTP_EVENT_ON_HEADER:
        break;
    case HTTP_EVENT_ON_DATA:
        if(count + evt->data_len < ((thttp_user_data *)evt->user_data)->data_len){
          memcpy(&((thttp_user_data *)evt->user_data)->data[count], (char *)evt->data, evt->data_len);
          count = count + evt->data_len;
          ((thttp_user_data *)evt->user_data)->data[count] = '\0';
        }
        break;
    case HTTP_EVENT_ON_FINISH:
        break;
    case HTTP_EVENT_DISCONNECTED:
        break;
    }
    return ESP_OK;
}


uint8_t update_dns_reg(char *ret, int len)
{
    thttp_user_data retData = {
        .data = ret,
        .data_len = len,
    };
    int retCode = 500;
    esp_http_client_config_t config = {
        .url = get_config_data()->dns_update_uri,
        .event_handler = client_handle,
        .cert_pem = get_config_data()->dns_cert,
        .user_data = &retData,
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);
    esp_err_t err = esp_http_client_perform(client);
    if (err == ESP_OK){
        retCode = esp_http_client_get_status_code(client);
    }
    esp_http_client_cleanup(client);
    return retCode == 200;
}