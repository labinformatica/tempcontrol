#include "serial_com.h"
#include "driver/uart.h"
#include <math.h>
#include "esp_log.h"


#define UART_RX_BUF_SIZE 256
#define MAX_RETRY 5
static uint8_t rxBuff[UART_RX_BUF_SIZE];
const uart_port_t uart_num = UART_NUM_2;

static uint8_t lamp_pwd  = 0;
static uint8_t cooler_pwd= 0;

void serial_init(void)
{
    uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB};
    // Configure UART parameters
    ESP_ERROR_CHECK(uart_param_config(uart_num, &uart_config));
    ESP_ERROR_CHECK(uart_set_pin(uart_num, 
                                 17, // Tx
                                 16, // Rx
                                 UART_PIN_NO_CHANGE, 
                                 UART_PIN_NO_CHANGE));
    ESP_ERROR_CHECK(uart_driver_install(uart_num, UART_RX_BUF_SIZE, 0, 0, NULL, ESP_INTR_FLAG_IRAM));
}

// CRC-8 - based on the CRC8 formulas by Dallas/Maxim
// code released under the therms of the GNU GPL 3.0 license
uint8_t CRC8(const uint8_t *data, size_t dataLength)
{
    uint8_t crc = 0x00;
    while (dataLength--)
    {
        uint8_t extract = *data++;
        for (uint8_t tempI = 8; tempI; tempI--)
        {
            uint8_t sum = (crc ^ extract) & 0x01;
            crc >>= 1;
            if (sum)
            {
                crc ^= 0x8C;
            }
            extract >>= 1;
        }
    }
    return crc;
}


void lamp_set_power(uint8_t pwd)
{
    serial_pkg sp;
    uint8_t k;
    sp.pkg.header = 0x0C;
    sp.pkg.cmd = LAMP_SET_POWER;
    for(k = 1; k < dataLen; k++)
      sp.pkg.data[k] = 0;
    sp.pkg.data[0] = pwd;
    sp.pkg.crc = CRC8(sp.bytes, pkgLen - 1);
    uart_write_bytes(uart_num, (const char *) sp.bytes, pkgLen);
    lamp_pwd = pwd;
}

uint8_t lamp_get_power(void)
{
    return lamp_pwd;
}

void lamp_poweroff(void)
{
    serial_pkg sp;
    uint8_t k;
    sp.pkg.header = 0x0C;
    sp.pkg.cmd = LAMP_POWER_OFF;
    for(k = 0; k < dataLen; k++)
      sp.pkg.data[k] = 0;
    sp.pkg.crc = CRC8(sp.bytes, pkgLen - 1);
    uart_write_bytes(uart_num, (const char *) sp.bytes, pkgLen);
    lamp_pwd = 0;
}

void cooler_set_power(uint8_t pwd)
{
    serial_pkg sp;
    uint8_t k;
    sp.pkg.header = 0x0C;
    sp.pkg.cmd = COOLER_SET_POWER;
    for(k = 0; k < dataLen; k++)
      sp.pkg.data[k] = 0;
    sp.pkg.data[0] = pwd;
    sp.pkg.crc = CRC8(sp.bytes, pkgLen - 1);
    uart_write_bytes(uart_num, (const char *) sp.bytes, pkgLen);
    cooler_pwd = pwd;
}

uint8_t cooler_get_power(void)
{
    return cooler_pwd;
}

void cooler_poweroff(void)
{
    serial_pkg sp;
    uint8_t k;
    sp.pkg.header = 0x0C;
    sp.pkg.cmd = COOLER_POWER_OFF;
    for(k = 0; k < dataLen; k++)
      sp.pkg.data[k] = 0;
    sp.pkg.crc = CRC8(sp.bytes, pkgLen - 1);
    uart_write_bytes(uart_num, (const char *) sp.bytes, pkgLen);
    cooler_pwd = 0;
}

uint16_t temp_get_value(void)
{
    uint16_t ret;
    uint8_t rcv_len = 0;
    serial_pkg sp;
    uint8_t k;
    uint8_t retry_cnt = 0;
    sp.pkg.header = 0x0C;
    sp.pkg.cmd = GET_TEMP;
    for(k = 0; k < dataLen; k++)
      sp.pkg.data[k] = 0;
    sp.pkg.crc = CRC8(sp.bytes, pkgLen - 1);
    uart_flush(uart_num);
    uart_write_bytes(uart_num, (const char *) sp.bytes, pkgLen);
    rcv_len = uart_read_bytes(uart_num, rxBuff, UART_RX_BUF_SIZE, 700 / portTICK_RATE_MS);
    ESP_LOGE("serial","Recibidos %d", rcv_len);
    while((rcv_len < pkgLen) && (retry_cnt < MAX_RETRY)){
        rcv_len += uart_read_bytes(uart_num, &rxBuff[rcv_len], UART_RX_BUF_SIZE - rcv_len, 5 / portTICK_RATE_MS);
        retry_cnt ++;
        ESP_LOGE("serial","Recibidos %d Retry: %d", rcv_len, retry_cnt);
    }
    
    if((retry_cnt < MAX_RETRY) && (rxBuff[0] == 0x0C) && (rxBuff[1] == GET_TEMP) && (rxBuff[pkgLen-1] == CRC8(rxBuff, pkgLen - 1))){
        ret = (rxBuff[2] << 8) | rxBuff[3];
    }
    else{
        ret = 0;
    }
    return ret;
}

double toCelsius(uint16_t adc)
{
    double Z1, Z2, Z3, Z4, Rt, ret;

    Rt = adc;
    Rt /= 32768.0;
    Rt *= 430.0;

    // Serial.print("\nResistance: "); Serial.println(Rt, 8);

    Z1 = -RTD_A;
    Z2 = RTD_A * RTD_A - (4 * RTD_B);
    Z3 = (4 * RTD_B) / 100;
    Z4 = 2 * RTD_B;

    ret = Z2 + (Z3 * Rt);
    ret = (sqrt(ret) + Z1) / Z4;

    if (ret >= 0)
        return ret;

    // ugh.
    Rt /= 100;
    Rt *= 100; // normalize to 100 ohm

    double rpoly = Rt;

    ret = -242.02;
    ret += 2.2228 * rpoly;
    rpoly *= Rt; // square
    ret += 2.5859e-3 * rpoly;
    rpoly *= Rt; // ^3
    ret -= 4.8260e-6 * rpoly;
    rpoly *= Rt; // ^4
    ret -= 2.8183e-8 * rpoly;
    rpoly *= Rt; // ^5
    ret += 1.5243e-10 * rpoly;

    return ret;
}