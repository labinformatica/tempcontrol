#include "config_file.h"
#include <stdio.h>

static config_data cfg_data;

config_data * get_config_data(void){
  return &cfg_data;
}

void load_config_data(void){
  FILE * f;
  f = fopen("/spiffs/config.bin", "rb");
  if(f != NULL){
      fread(&cfg_data, sizeof(cfg_data), 1, f);
      fclose(f);
  }
  else{
      strcpy(cfg_data.desc, "[Description is not set]");
      cfg_data.dns_update_uri[0] = '\0';
      cfg_data.dns_cert[0] = '\0';
      cfg_data.dns_update_enable = false;
  }

}

void save_config_data(void){
  FILE * f;
  f = fopen("/spiffs/config.bin", "wb");
  if(f != NULL){
      fwrite(&cfg_data, sizeof(cfg_data), 1, f);
      fclose(f);
  }
}