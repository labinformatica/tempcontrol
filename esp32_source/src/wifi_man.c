#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"

#include "lwip/err.h"
#include "lwip/sys.h"
#include "wifi_man.h"
#include "httpd.h"
#include "freertos/queue.h"
#include "dns_reg.h"
#include "config_file.h"

#define EXAMPLE_ESP_MAXIMUM_RETRY 10

static esp_netif_t *ap_netif = NULL;
static esp_netif_t *sta_netif = NULL;

wifi_ap_record_t ap_info[MAX_SCAN_NETS];

static int s_retry_num = 0;

static void event_handler_sta(void* arg, esp_event_base_t event_base,
                                int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START) {
        esp_wifi_connect();
    } else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED) {
        if (s_retry_num < EXAMPLE_ESP_MAXIMUM_RETRY) {
            esp_wifi_connect();
            s_retry_num++;
            ESP_LOGI("sta", "retry to connect to the AP");
        } 
        else
            ESP_LOGI("sta","connect to the AP fail");
    } else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP) {
        ip_event_got_ip_t* event = (ip_event_got_ip_t*) event_data;
        ESP_LOGI("sta", "got ip:" IPSTR, IP2STR(&event->ip_info.ip));
        if(get_config_data()->dns_update_enable)
          update_dns_reg(NULL, 0);
    }
}

void wifi_setup_ap(void){
    uint8_t macAddr[6];
    
    char* ip= "172.16.32.1";
    char* gateway = "172.16.32.1";
    char* netmask = "255.255.255.0";

    esp_netif_ip_info_t info_t;
    memset(&info_t, 0, sizeof(esp_netif_ip_info_t));
    
    ESP_ERROR_CHECK(esp_netif_init());
    ESP_ERROR_CHECK(esp_event_loop_create_default());
    sta_netif = esp_netif_create_default_wifi_sta();
    ap_netif =esp_netif_create_default_wifi_ap();
    esp_netif_dhcps_stop(ap_netif);
    
    info_t.ip.addr = esp_ip4addr_aton((const char *)ip);
    info_t.netmask.addr = esp_ip4addr_aton((const char *)netmask);
    info_t.gw.addr = esp_ip4addr_aton((const char *)gateway);
    esp_netif_set_ip_info(ap_netif, &info_t);    
    ESP_ERROR_CHECK(esp_netif_dhcps_start(ap_netif));
    
    wifi_init_config_t cfg_ap = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg_ap));
  /*  ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        app_callback,
                                                        NULL,
                                                        NULL));*/
      ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                          ESP_EVENT_ANY_ID,
                                                          &event_handler_sta,
                                                          NULL,
                                                          NULL));
      ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                          IP_EVENT_STA_GOT_IP,
                                                          &event_handler_sta,
                                                          NULL,
                                                          NULL));  
    ESP_ERROR_CHECK(esp_wifi_get_mac(WIFI_IF_AP, macAddr));
    
    wifi_config_t wifi_config;
    wifi_config.ap.channel = 12;
    wifi_config.ap.max_connection = 10;
    wifi_config.ap.authmode = WIFI_AUTH_OPEN;
    snprintf((char *)wifi_config.ap.ssid, 32,"ESP32_%02X%02X%02X%02X%02X%02X", macAddr[0], macAddr[1], macAddr[2], macAddr[3], macAddr[4], macAddr[5]);
    wifi_config.ap.ssid_len = strlen((char *)wifi_config.ap.ssid);
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_APSTA));
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &wifi_config));    
    ESP_ERROR_CHECK(esp_wifi_start());
}

void wifi_setup_sta(char *ssid, char * pwd){
    wifi_config_t wifi_config;
    if(ssid != NULL){
      memset(&wifi_config, 0, sizeof(wifi_config_t));
      strcpy((char *)wifi_config.sta.ssid, (char *)ssid);
      strcpy((char *)wifi_config.sta.password, (char *)pwd);
      wifi_config.sta.scan_method = WIFI_ALL_CHANNEL_SCAN;
      ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));  
      /* ESP_ERROR_CHECK(esp_wifi_stop()); */
      s_retry_num = 0;
      ESP_ERROR_CHECK(esp_wifi_disconnect());
    }
    esp_wifi_get_config(WIFI_IF_STA, &wifi_config);
    if (strlen((char *)wifi_config.sta.ssid) != 0)
        ESP_ERROR_CHECK(esp_wifi_connect());
}

void wifi_scan(uint16_t *detectedCnt, wifi_ap_record_t *info[]){
    uint16_t cnt = MAX_SCAN_NETS;
    uint16_t found=0;
    memset(ap_info, 0, sizeof(ap_info)); 
    esp_wifi_scan_start(NULL, true);
    ESP_ERROR_CHECK(esp_wifi_scan_get_ap_records(&cnt, ap_info));
    ESP_ERROR_CHECK(esp_wifi_scan_get_ap_num(&found));
    *detectedCnt = found < MAX_SCAN_NETS ? found : MAX_SCAN_NETS;
    *info = ap_info;
}

void wifi_sta_info(esp_netif_ip_info_t *ip_info){
    ESP_ERROR_CHECK(esp_netif_get_ip_info(sta_netif, ip_info));
}