#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "driver/gpio.h"
#include "sdkconfig.h"
#include "nvs_flash.h"
#include "esp_log.h"
#include "wifi_man.h"
#include "httpd.h"
#include "esp_spiffs.h"
#include "esp_err.h"
#include "driver/gpio.h"
#include "config_file.h"
#include "serial_com.h"


const esp_vfs_spiffs_conf_t conf = {
      .base_path = "/spiffs",
      .partition_label = NULL,
      .max_files = 5,
      .format_if_mount_failed = false
    };


void app_main()
{    
    esp_err_t err = nvs_flash_init();
    if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_ERROR_CHECK(nvs_flash_erase());
        err = nvs_flash_init();
    }
    vTaskDelay(1000 / portTICK_PERIOD_MS);

    esp_err_t ret = esp_vfs_spiffs_register(&conf);    

    
    if (ret != ESP_OK) {
        if (ret == ESP_FAIL) {
            ESP_LOGE("Mount", "Failed to mount or format filesystem");
        } else if (ret == ESP_ERR_NOT_FOUND) {
            ESP_LOGE("Mount", "Failed to find SPIFFS partition");
        } else {
            ESP_LOGE("Mount", "Failed to initialize SPIFFS (%s)", esp_err_to_name(ret));
        }
        return;
    }
    serial_init();
    wifi_setup_ap();
    wifi_setup_sta(NULL, NULL);
    setup_http();
    load_config_data();
    ESP_LOGI("boot", "Configuración finalizada");    
}