#include "httpd.h"
#include <esp_http_server.h>
#include "esp_log.h"
#include "wifi_man.h"
#include <string.h>
#include "esp_netif_ip_addr.h"
#include "version.h"
#include "esp_https_ota.h"
#include "esp_tls.h"
#include "esp_http_client.h"
#include "fw_url.h"
#include "cJSON.h"
#include "config_file.h"
#include "dns_reg.h"
#include "serial_com.h"

#define BUFF_SIZE 512
static httpd_handle_t server = NULL;

extern const uint8_t index_html_start[] asm("_binary_index_html_start");
extern const uint8_t index_html_end[] asm("_binary_index_html_end");
extern const uint8_t bootstrap_css_start[] asm("_binary_bootstrap_css_start");
extern const uint8_t bootstrap_css_end[] asm("_binary_bootstrap_css_end");
extern const uint8_t grid_css_start[] asm("_binary_grid_css_start");
extern const uint8_t grid_css_end[] asm("_binary_grid_css_end");
extern const uint8_t reboot_css_start[] asm("_binary_reboot_css_start");
extern const uint8_t reboot_css_end[] asm("_binary_reboot_css_end");
extern const uint8_t utilities_css_start[] asm("_binary_utilities_css_start");
extern const uint8_t utilities_css_end[] asm("_binary_utilities_css_end");
extern const uint8_t logo_jpg_start[] asm("_binary_logo_jpg_start");
extern const uint8_t logo_jpg_end[] asm("_binary_logo_jpg_end");
extern const uint8_t bootstrap_bundle_min_js_start[] asm("_binary_bootstrap_bundle_min_js_start");
extern const uint8_t bootstrap_bundle_min_js_end[] asm("_binary_bootstrap_bundle_min_js_end");
extern const uint8_t bootstrap_esm_min_js_start[] asm("_binary_bootstrap_esm_min_js_start");
extern const uint8_t bootstrap_esm_min_js_end[] asm("_binary_bootstrap_esm_min_js_end");
extern const uint8_t bootstrap_min_js_start[] asm("_binary_bootstrap_min_js_start");
extern const uint8_t bootstrap_min_js_end[] asm("_binary_bootstrap_min_js_end");
extern const uint8_t chart_min_js_start[] asm("_binary_chart_min_js_start");
extern const uint8_t chart_min_js_end[] asm("_binary_chart_min_js_end");
extern const uint8_t helpers_esm_js_start[] asm("_binary_helpers_esm_js_start");
extern const uint8_t helpers_esm_js_end[] asm("_binary_helpers_esm_js_end");
extern const uint8_t jquery_js_start[] asm("_binary_jquery_3_6_0_min_js_start");
extern const uint8_t jquery_js_end[] asm("_binary_jquery_3_6_0_min_js_end");
extern const uint8_t gitlab_cert_start[] asm("_binary_gitlab_com_pem_start");
extern const uint8_t gitlab_cert_end[] asm("_binary_gitlab_com_pem_end");


/*
  Utility functions
*/

uint8_t hex2dec(char c){
    uint8_t ret = 0;
    if ((c >= '0') && (c <= '9'))
        ret = c - '0';
    else if ((c >= 'A') && (c <= 'F'))
        ret = c - 'A' + 10;
    else if ((c >= 'a') && (c <= 'f'))
        ret = c - 'a' + 10;
    return ret;
}
/*
  Escape c string for coding as json string. The str argument must be a null terminate string (c
  standar string) This function must add chars to the string, so the len argument represent the 
  storage size of array.  
*/
void _insert(char *a, int p){
    int lastPos = strlen(a);
    while(lastPos >= p){
        a[lastPos + 1] = a[lastPos];
        lastPos--;
    }
}
void json_esc_str(char *str, int len){
    int logic_size = strlen(str);
    int k=0;
    while ((str[k] != '\0') && (logic_size < len)){
        switch(str[k]){
            case '\\':
            case '"':
            case '/':
              _insert(str, k);
              str[k] = '\\';
              k++;
            break;

        case '\b':
            _insert(str, k);
            str[k] = '\\';
            str[k+1] = 'b';
            k++;
            break;

        case '\t':
            _insert(str, k);
            str[k] = '\\';
            str[k+1] = 't';
            k++;
            break;

        case '\n':
            _insert(str, k);
            str[k] = '\\';
            str[k+1] = 'n';
            k++;
            break;

        case '\f':
            _insert(str, k);
            str[k] = '\\';
            str[k+1] = 'f';
            k++;
            break;

        case '\r':
            _insert(str, k);
            str[k] = '\\';
            str[k+1] = 'r';
            k++;
            break;
        }
        k++;
        logic_size = strlen(str);
    }
}

int parseRequest(char *str, char *field[], char *values[], int len){
    unsigned k;
    int pCnt = 0;
    unsigned sLen = strlen(str); //ssid=valor1&clave=valor2
    field[0] = &str[0];
    for (k = 0; k < sLen; k++)
    {
        if (str[k] == '=')
        {
            str[k] = '\0';
            values[pCnt] = &str[k + 1];
            pCnt = pCnt + 1;
        }
        if (str[k] == '&')
        {
            str[k] = '\0';
            field[pCnt] = &str[k + 1];
        }
    }
    return pCnt;
}

void urlDecode(char *str){
    int k;
    int j;
    int len = strlen(str);
    for (k = 0; k <= len; k++)
    {
        if (str[k] == '%')
        {
            str[k] = (hex2dec(str[k + 1]) << 4) | hex2dec(str[k + 2]);
            for (j = k + 1; j <= len - 2; j++)
                str[j] = str[j + 2];
            len = len - 2;
        }
    }
}

/*
  Getters for the pages stored in flash
*/

esp_err_t get_index(httpd_req_t *req){
    httpd_resp_set_type(req, "text/html");
    httpd_resp_send(req, (const char *) index_html_start,  index_html_end - index_html_start);
    return ESP_OK;
}

esp_err_t get_css_bootstrap(httpd_req_t *req){
    httpd_resp_set_type(req, "text/css");
    httpd_resp_send(req, (const char *)bootstrap_css_start, bootstrap_css_end - bootstrap_css_start);
    return ESP_OK;
}

esp_err_t get_css_grid(httpd_req_t *req){
    httpd_resp_set_type(req, "text/css");
    httpd_resp_send(req, (const char *)grid_css_start, grid_css_end - grid_css_start);
    return ESP_OK;
}

esp_err_t get_css_reboot(httpd_req_t *req){
    httpd_resp_set_type(req, "text/css");
    httpd_resp_send(req, (const char *)reboot_css_start, reboot_css_end - reboot_css_start);
    return ESP_OK;
}

esp_err_t get_css_utilities(httpd_req_t *req){
    httpd_resp_set_type(req, "text/css");
    httpd_resp_send(req, (const char *)utilities_css_start, utilities_css_end - utilities_css_start);
    return ESP_OK;
}

esp_err_t get_img_logo(httpd_req_t *req){
    httpd_resp_set_type(req, "image/jpeg");
    httpd_resp_send(req, (const char *)logo_jpg_start, logo_jpg_end - logo_jpg_start);
    return ESP_OK;
}

esp_err_t get_js_bootstrap_boundle(httpd_req_t *req){
    httpd_resp_set_type(req, "text/javascript");
    httpd_resp_send(req, (const char *)bootstrap_bundle_min_js_start, bootstrap_bundle_min_js_end - bootstrap_bundle_min_js_start);
    return ESP_OK;
}

esp_err_t get_js_bootstrap_esm(httpd_req_t *req){
    httpd_resp_set_type(req, "text/javascript");
    httpd_resp_send(req, (const char *)bootstrap_esm_min_js_start, bootstrap_esm_min_js_end - bootstrap_esm_min_js_start);
    return ESP_OK;
}

esp_err_t get_js_bootstrap_js(httpd_req_t *req){
    httpd_resp_set_type(req, "text/javascript");
    httpd_resp_send(req, (const char *)bootstrap_min_js_start, bootstrap_min_js_end - bootstrap_min_js_start);
    return ESP_OK;
}

esp_err_t get_js_jquery(httpd_req_t *req){
    httpd_resp_set_type(req, "text/javascript");
    httpd_resp_send(req, (const char *)jquery_js_start, jquery_js_end - jquery_js_start);
    return ESP_OK;
}

esp_err_t get_js_chart(httpd_req_t *req){
    httpd_resp_set_type(req, "text/javascript");
    httpd_resp_send(req, (const char *)chart_min_js_start, chart_min_js_end - chart_min_js_start);
    return ESP_OK;
}

esp_err_t get_js_helpers(httpd_req_t *req){
    httpd_resp_set_type(req, "text/javascript");
    httpd_resp_send(req, (const char *)helpers_esm_js_start, helpers_esm_js_end - helpers_esm_js_start);
    return ESP_OK;
}

esp_err_t get_favicon(httpd_req_t *req){
    httpd_resp_set_type(req, "image/jpeg");
    httpd_resp_send(req, (const char *)logo_jpg_start, logo_jpg_end - logo_jpg_start);
    return ESP_OK;
}


/* Old getters  */
esp_err_t get_netList(httpd_req_t *req)
{
    char buff[BUFF_SIZE];
    int k;
    int quality;
    uint16_t cnt;
    wifi_ap_record_t *ap_info;
    wifi_scan(&cnt, &ap_info);
    httpd_resp_set_type(req, "application/json");
    snprintf(buff, BUFF_SIZE, "{\"count\":%d,\"ssid\":[", cnt);
    httpd_resp_send_chunk(req, buff, HTTPD_RESP_USE_STRLEN);
    for (k = 0; k < cnt; k++){
        if (k == cnt - 1)
            snprintf(buff, BUFF_SIZE, "\"%s\"],", ap_info[k].ssid);
        else
            snprintf(buff, BUFF_SIZE, "\"%s\",", ap_info[k].ssid);
        httpd_resp_send_chunk(req, buff, HTTPD_RESP_USE_STRLEN);
    }
    strcpy(buff, "\"power\":[");
    httpd_resp_send_chunk(req, buff, HTTPD_RESP_USE_STRLEN);
    for (k = 0; k < cnt; k++){
        if (ap_info[k].rssi <= -100)
            quality = 0;
        else if (ap_info[k].rssi >= -50)
            quality = 100;
        else
            quality = 2 * (ap_info[k].rssi + 100);
        if (k == cnt - 1)
            snprintf(buff, BUFF_SIZE, "%d]}", quality);
        else
            snprintf(buff, BUFF_SIZE, "%d,", quality);
        httpd_resp_send_chunk(req, buff, HTTPD_RESP_USE_STRLEN);
    }
    httpd_resp_send_chunk(req, 0, 0);
    return ESP_OK;
}

esp_err_t get_netStatus(httpd_req_t *req){
    char buff[256];
    uint32_t pot;
    esp_netif_ip_info_t ip_info;
    wifi_config_t sta;
    float temp;
    uint16_t adc;
    adc = temp_get_value();
    temp = toCelsius(adc);
    httpd_resp_set_type(req, "application/json");
    ESP_ERROR_CHECK(esp_wifi_get_config(WIFI_IF_STA, &sta));
    wifi_sta_info(&ip_info);
    pot = lamp_get_power();
    snprintf(buff, 255, "{ \"ip\": \"%d.%d.%d.%d\", \"msk\": \"%d.%d.%d.%d\", \"gw\":\"%d.%d.%d.%d\", \"ssid\": \"%s\", \"temp\":%.2f, \"pot\": %u}", IP2STR(&ip_info.ip), IP2STR(&ip_info.netmask), IP2STR(&ip_info.gw), sta.sta.ssid, temp, pot);

    httpd_resp_send_chunk(req, buff, HTTPD_RESP_USE_STRLEN);
    httpd_resp_send_chunk(req, 0, 0);
    return ESP_OK;
}

esp_err_t get_fwversion(httpd_req_t *req){
    char buff[BUFF_SIZE];
    httpd_resp_set_type(req, "application/json");
    snprintf(buff, BUFF_SIZE, "{ \"fwver\": \"%s\", \"date\": \"%s\", \"time\": \"%s\"}", fwver, build_date, build_time);
    httpd_resp_send(req, buff, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

esp_err_t https_client_fw(esp_http_client_event_t *evt)
{
    static int output_len = 0;       // Stores number of bytes read
    int mbedtls_err;
    esp_err_t err;
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            break;
        case HTTP_EVENT_ON_CONNECTED:
            break;
        case HTTP_EVENT_HEADER_SENT:
            break;
        case HTTP_EVENT_ON_HEADER:
            break;
        case HTTP_EVENT_ON_DATA:
            if (!esp_http_client_is_chunked_response(evt->client)) {
                if (evt->user_data && (evt->data_len + output_len < 127)) {
                    memcpy(evt->user_data + output_len, evt->data, evt->data_len);
                    output_len += evt->data_len;
                } 
            }
            break;
        case HTTP_EVENT_ON_FINISH:
            output_len = 0;
            break;
        case HTTP_EVENT_DISCONNECTED:
            mbedtls_err = 0;
            err = esp_tls_get_and_clear_last_error(evt->data, &mbedtls_err, NULL);
            if (err != 0) {
                output_len = 0;
            }
            break;
    }
    return ESP_OK;
}

esp_err_t get_fwavailable(httpd_req_t *req){
    char buff[BUFF_SIZE];
    char json[128];
    esp_http_client_config_t config = {
        .host = FW_INFO_HOST,
        .path = FW_INFO_PATH,
        .transport_type = HTTP_TRANSPORT_OVER_SSL,
        .event_handler = https_client_fw,
        .cert_pem = (const char *)gitlab_cert_start,
        .user_data = json,
    };
    esp_http_client_handle_t client = esp_http_client_init(&config);
    esp_err_t err = esp_http_client_perform(client);

    if ((err == ESP_OK) && (esp_http_client_get_status_code(client) == 200)) {
        strncpy(buff, json, 127);
    } else {
        strcpy(buff, "{\"fwVer\": \"[No disponible]\",\"fwDate\": \"[No disponible]\",\"fwTime\": \"[No disponible]\"}");
    }
    esp_http_client_cleanup(client);

    httpd_resp_set_type(req, "application/json");
    httpd_resp_send_chunk(req, buff, HTTPD_RESP_USE_STRLEN);
    httpd_resp_send_chunk(req, 0, 0);
    return ESP_OK;
}

/* Rest API (only use post and get verbs for back compatibility with octave urlread function) */

esp_err_t api_temp_get(httpd_req_t *req){
    char buff[BUFF_SIZE];
    float temp;
    uint32_t adc;
    adc = temp_get_value();
    temp = toCelsius(adc);
    httpd_resp_set_type(req, "application/json");
    snprintf(buff, BUFF_SIZE, "{ \"temp\": %f, \"adc\": %u}", temp, adc);
    httpd_resp_send(req, buff, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

esp_err_t api_lamp_get(httpd_req_t *req){
    char buff[BUFF_SIZE];
    uint32_t pwr;
    pwr = lamp_get_power();
    httpd_resp_set_type(req, "application/json");
    snprintf(buff, BUFF_SIZE, "{ \"power\": %d}", pwr);
    httpd_resp_send(req, buff, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

esp_err_t api_lamp_set(httpd_req_t *req){
    int token_len;
    cJSON * json_arg;
    cJSON * json_arg_pwr;
    char response[256];
    char * raw_start;
    int total_len = req->content_len;
    int cur_len = 0;
    char buf[BUFF_SIZE] ;
    int received = 0;
    httpd_resp_set_type(req, "application/json");
    
    if (total_len >= BUFF_SIZE) {
        httpd_resp_set_status(req, "413 Request Too Large");
        snprintf(response, sizeof(response), "{ \"power\": %d, \"status\": \"fail\", \"error\":\"Content too long.\"}", lamp_get_power());
        httpd_resp_send(req, response, HTTPD_RESP_USE_STRLEN);
        return ESP_OK;
    }
    while (cur_len < total_len) {
        received = httpd_req_recv(req, buf + cur_len, total_len);
        if (received <= 0) {
            httpd_resp_set_status(req, "400 Bad Request");
            snprintf(response, sizeof(response), "{ \"power\": %d, \"status\": \"fail\", \"error\":\"Failed to post lamp power value.\"}", lamp_get_power());
            httpd_resp_send(req, response, HTTPD_RESP_USE_STRLEN);
            return ESP_OK;
        }
        cur_len += received;
    }
    buf[total_len] = '\0';
    ESP_LOGE("POST Request", "%s", buf);
    /* 
      If the raw buffer start with "json" string the data has been send as a pair key/values,
      the key is the json text and the value is the json string. 
    */
   token_len = strlen("json=");
   raw_start = buf;
    if (strncmp("json=", buf, token_len) == 0){
        if(total_len == token_len){
            httpd_resp_set_status(req, "400 Bad Request");
            snprintf(response, sizeof(response), "{ \"power\": %d, \"status\": \"fail\", \"error\" : \"Malformed string: starts with the identifier json but does not include any argument of this type.\"}", lamp_get_power());
            httpd_resp_send(req, response, HTTPD_RESP_USE_STRLEN);
            return ESP_OK;
        }
        urlDecode(&buf[token_len]);
        raw_start = &buf[token_len];
    }
    json_arg = cJSON_Parse(raw_start);
    if(json_arg == NULL){
        httpd_resp_set_status(req, "400 Bad Request");
        snprintf(response, sizeof(response), "{ \"power\": %d, \"status\": \"fail\", \"error\":\"Malformed string: error in json string.\"}", lamp_get_power());
        httpd_resp_send(req, response, HTTPD_RESP_USE_STRLEN);
        return ESP_OK;
    }
    json_arg_pwr = cJSON_GetObjectItem(json_arg, "power");
    if (!cJSON_IsNumber(json_arg_pwr)){
        cJSON_Delete(json_arg);
        httpd_resp_set_status(req, "400 Bad Request");
        snprintf(response, sizeof(response), "{ \"power\": %d, \"status\": \"fail\", \"error\":\"Malformed string: power value is not a number.\"}", lamp_get_power());
        httpd_resp_send(req, response, HTTPD_RESP_USE_STRLEN);
        return ESP_OK;
    }
    if((json_arg_pwr->valueint < 0) || (json_arg_pwr->valueint > 100)){
        cJSON_Delete(json_arg);
        httpd_resp_set_status(req, "400 Bad Request");
        snprintf(response, sizeof(response), "{ \"power\": %d, \"status\": \"fail\", \"error\":\"Malformed string: power value out of range.\"}", lamp_get_power());
        httpd_resp_send(req, response, HTTPD_RESP_USE_STRLEN);
        return ESP_OK;
    }

    lamp_set_power(json_arg_pwr->valueint);
    snprintf(response, sizeof(response), "{ \"power\": %d, \"status\": \"ok\", \"error\":\"\"}", lamp_get_power());
    httpd_resp_set_status(req, "202 Accepted");
    httpd_resp_send(req, response, HTTPD_RESP_USE_STRLEN);
    cJSON_Delete(json_arg);
    return ESP_OK;
}

esp_err_t api_cooler_get(httpd_req_t *req){
    char buff[BUFF_SIZE];
    uint32_t pwr = cooler_get_power();
    httpd_resp_set_type(req, "application/json");
    snprintf(buff, BUFF_SIZE, "{ \"power\": %u}", pwr);
    httpd_resp_send(req, buff, HTTPD_RESP_USE_STRLEN);
    return ESP_OK;
}

esp_err_t api_cooler_set(httpd_req_t *req){
    int token_len;
    cJSON * json_arg;
    cJSON * json_arg_pwr;
    char response[256];
    char * raw_start;
    int total_len = req->content_len;
    int cur_len = 0;
    char buf[BUFF_SIZE] ;
    int received = 0;
    httpd_resp_set_type(req, "application/json");
    
    if (total_len >= BUFF_SIZE) {
        httpd_resp_set_status(req, "413 Request Too Large");
        snprintf(response, sizeof(response), "{ \"power\": %d, \"status\": \"fail\", \"error\":\"Content too long.\"}", cooler_get_power());
        httpd_resp_send(req, response, HTTPD_RESP_USE_STRLEN);
        return ESP_OK;
    }
    while (cur_len < total_len) {
        received = httpd_req_recv(req, buf + cur_len, total_len);
        if (received <= 0) {
            httpd_resp_set_status(req, "400 Bad Request");
            snprintf(response, sizeof(response), "{ \"power\": %d, \"status\": \"fail\", \"error\":\"Failed to post lamp power value.\"}", cooler_get_power());
            httpd_resp_send(req, response, HTTPD_RESP_USE_STRLEN);
            return ESP_OK;
        }
        cur_len += received;
    }
    buf[total_len] = '\0';
    /* 
      If the raw buffer start with "json" string the data has been send as a pair key/values,
      the key is the json text and the value is the json string. 
    */
   token_len = strlen("json=");
   raw_start = buf;
    if (strncmp("json=", buf, token_len) == 0){
        if(total_len == token_len){
            httpd_resp_set_status(req, "400 Bad Request");
            snprintf(response, sizeof(response), "{ \"power\": %d, \"status\": \"fail\", \"error\" : \"Malformed string: starts with the identifier json but does not include any argument of this type.\"}", cooler_get_power());
            httpd_resp_send(req, response, HTTPD_RESP_USE_STRLEN);
            return ESP_OK;
        }
        urlDecode(&buf[token_len]);
        raw_start = &buf[token_len];
    }
    json_arg = cJSON_Parse(raw_start);
    if(json_arg == NULL){
        httpd_resp_set_status(req, "400 Bad Request");
        snprintf(response, sizeof(response), "{ \"power\": %d, \"status\": \"fail\", \"error\":\"Malformed string: error in json string.\"}", cooler_get_power());
        httpd_resp_send(req, response, HTTPD_RESP_USE_STRLEN);
        return ESP_OK;
    }
    json_arg_pwr = cJSON_GetObjectItem(json_arg, "power");
    if (!cJSON_IsNumber(json_arg_pwr)){
        cJSON_Delete(json_arg);
        httpd_resp_set_status(req, "400 Bad Request");
        snprintf(response, sizeof(response), "{ \"power\": %d, \"status\": \"fail\", \"error\":\"Malformed string: power value is not a number.\"}", cooler_get_power());
        httpd_resp_send(req, response, HTTPD_RESP_USE_STRLEN);
        return ESP_OK;
    }
    if((json_arg_pwr->valueint < 0) || (json_arg_pwr->valueint > 100)){
        cJSON_Delete(json_arg);
        httpd_resp_set_status(req, "400 Bad Request");
        snprintf(response, sizeof(response), "{ \"power\": %d, \"status\": \"fail\", \"error\":\"Malformed string: power value out of range.\"}", cooler_get_power());
        httpd_resp_send(req, response, HTTPD_RESP_USE_STRLEN);
        return ESP_OK;
    }

    cooler_set_power(json_arg_pwr->valueint);
    snprintf(response, sizeof(response), "{ \"power\": %d,  \"status\": \"ok\", \"error\":\"\"}", cooler_get_power());
    httpd_resp_set_status(req, "202 Accepted");
    httpd_resp_send(req, response, HTTPD_RESP_USE_STRLEN);
    cJSON_Delete(json_arg);
    return ESP_OK;
}

esp_err_t api_desc_get(httpd_req_t *req){
    char * response_str;
    cJSON *response = cJSON_CreateObject();
    cJSON *desc;
    if(response == NULL){
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Internal server error (not memory to allocate response)");
        cJSON_Delete(response);
        return ESP_OK;
    }
    desc = cJSON_CreateString(get_config_data()->desc);
    if(desc == NULL){
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Internal server error (not memory to allocate response)");
        cJSON_Delete(response);
        return ESP_OK;
    }
    cJSON_AddItemToObject(response, "desc", desc);
    response_str = cJSON_Print(response);
    httpd_resp_set_type(req, "application/json");
    httpd_resp_send(req, response_str, HTTPD_RESP_USE_STRLEN);
    cJSON_Delete(response);
    free (response_str);
    return ESP_OK;
}

esp_err_t api_desc_set(httpd_req_t *req){
    int token_len;
    cJSON * json_arg;
    cJSON * json_arg_desc;
    char * raw_start;
    int total_len = req->content_len;
    int cur_len = 0;
    char buf[BUFF_SIZE] ;
    int received = 0;
    httpd_resp_set_type(req, "application/json");
    
    if (total_len >= BUFF_SIZE) {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Data too long");
        return ESP_OK;
    }
    while (cur_len < total_len) {
        received = httpd_req_recv(req, buf + cur_len, total_len);
        if (received <= 0) {
            httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Incomplete data");
            return ESP_OK;
        }
        cur_len += received;
    }
    buf[total_len] = '\0';
    /* 
      If the raw buffer start with "json" string the data has been send as a pair key/values,
      the key is the json text and the value is the json string. 
    */
   token_len = strlen("json=");
   raw_start = buf;
    if (strncmp("json=", buf, token_len) == 0){
        if(total_len == token_len){
            httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Start with json token but not include the json string");
            return ESP_OK;
        }
        urlDecode(&buf[token_len]);
        raw_start = &buf[token_len];
    }
    json_arg = cJSON_Parse(raw_start);
    if(json_arg == NULL){
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Error in json data structure");
        return ESP_OK;
    }
    json_arg_desc = cJSON_GetObjectItem(json_arg, "desc");
    if (!cJSON_IsString(json_arg_desc) ||(json_arg_desc->valuestring == NULL)){
        cJSON_Delete(json_arg);
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "Incomplete data (does not include the attribute 'desc')");
        return ESP_OK;
    }
    httpd_resp_set_status(req, "202 Accepted");
    httpd_resp_sendstr(req, "Description was changed and saved");
    strcpy(get_config_data()->desc, json_arg_desc->valuestring);
    save_config_data();
    cJSON_Delete(json_arg);
    return ESP_OK;
}

esp_err_t api_dns_cfg_get(httpd_req_t *req){
    char * response_str;
    cJSON *response = cJSON_CreateObject();
    cJSON *dns_uri;
    cJSON *dns_en;
    cJSON *dns_cert;
    if(response == NULL){
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Internal server error (not memory to allocate response)");
        cJSON_Delete(response);
        return ESP_OK;
    }
    dns_uri = cJSON_CreateString(get_config_data()->dns_update_uri);
    if(dns_uri == NULL){
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Internal server error (not memory to allocate response)");
        cJSON_Delete(response);
        return ESP_OK;
    }
    cJSON_AddItemToObject(response, "uri", dns_uri);

    dns_en = cJSON_CreateBool(get_config_data()->dns_update_enable);
    if(dns_en == NULL){
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Internal server error (not memory to allocate response)");
        cJSON_Delete(response);
        return ESP_OK;
    }
    cJSON_AddItemToObject(response, "enable", dns_en);

    dns_cert = cJSON_CreateString(get_config_data()->dns_cert);
    if(dns_cert == NULL){
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "Internal server error (not memory to allocate response)");
        cJSON_Delete(response);
        return ESP_OK;
    }
    cJSON_AddItemToObject(response, "cert", dns_cert);

    response_str = cJSON_Print(response);
    httpd_resp_set_type(req, "application/json");
    httpd_resp_send(req, response_str, HTTPD_RESP_USE_STRLEN);
    cJSON_Delete(response);
    free (response_str);
    return ESP_OK;
}

esp_err_t api_dns_cfg_set(httpd_req_t *req){
    int token_len;
    cJSON * json_arg;
    cJSON * json_arg_uri;
    cJSON * json_arg_en;
    cJSON * json_arg_cert;
    char * raw_start;
    char dsnSrv[256];
    int total_len = req->content_len;
    int cur_len = 0;
    int ret;
    char buf[HTTPD_MAX_URI_LEN + MAX_DNS_CERT + 64] ;
    int received = 0;
    httpd_resp_set_type(req, "application/json");
    
    if (total_len >= sizeof(buf)) {
        httpd_resp_send_err(req, HTTPD_500_INTERNAL_SERVER_ERROR, "{\"message\" : \"Data too long\"}");
        return ESP_OK;
    }
    while (cur_len < total_len) {
        received = httpd_req_recv(req, buf + cur_len, total_len);
        if (received <= 0) {
            httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "{\"message\" : \"Incomplete data\"}");
            return ESP_OK;
        }
        cur_len += received;
    }
    buf[total_len] = '\0';
    /* 
      If the raw buffer start with "json" string the data has been send as a pair key/values,
      the key is the json text and the value is the json string. 
    */
   token_len = strlen("json=");
   raw_start = buf;
    if (strncmp("json=", buf, token_len) == 0){
        if(total_len == token_len){
            httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "{\"message\" : \"Start with json token but not include the json string\"}");
            return ESP_OK;
        }
        urlDecode(&buf[token_len]);
        raw_start = &buf[token_len];
    }
    json_arg = cJSON_Parse(raw_start);
    if(json_arg == NULL){
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "{\"message\" : \"Error in json data structure\"}");
        return ESP_OK;
    }
    json_arg_uri = cJSON_GetObjectItem(json_arg, "uri");
    if (!cJSON_IsString(json_arg_uri) ||(json_arg_uri->valuestring == NULL)){
        cJSON_Delete(json_arg);
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "{\"message\" : \"Incomplete data, does not include the attribute uri.\"}");
        return ESP_OK;
    }
    json_arg_en = cJSON_GetObjectItem(json_arg, "enable");
    if (!cJSON_IsBool(json_arg_en)){
        cJSON_Delete(json_arg);
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "{\"message\" : \"Incomplete data, does not include the attribute enable.\"}");
        return ESP_OK;
    }
    json_arg_cert = cJSON_GetObjectItem(json_arg, "cert");
    if (!cJSON_IsString(json_arg_cert)){
        cJSON_Delete(json_arg);
        httpd_resp_send_err(req, HTTPD_400_BAD_REQUEST, "{\"message\" : \"Incomplete data, does not include the attribute cert.\"}");
        return ESP_OK;
    }

    httpd_resp_set_status(req, "202 Accepted");
    strncpy(get_config_data()->dns_update_uri, json_arg_uri->valuestring, HTTPD_MAX_URI_LEN);
    strncpy(get_config_data()->dns_cert, json_arg_cert->valuestring, MAX_DNS_CERT);
    get_config_data()->dns_update_enable = json_arg_en->valueint;
    save_config_data();
    cJSON_Delete(json_arg);
    if(get_config_data()->dns_update_enable){
        ret = update_dns_reg(dsnSrv, sizeof(dsnSrv));
        json_esc_str(dsnSrv, sizeof(dsnSrv));
        if(ret){
            snprintf(buf, sizeof(buf), "{\"message\" : \"DNS config was changed and saved. The register process was successful! Server say: %s\"}", dsnSrv);
            httpd_resp_sendstr(req, buf);
        }
        else{
            snprintf(buf, sizeof(buf), "{\"message\" : \"DNS config was changed and saved, but the register process has fail. Check the url or ssl cert. Server say %s\"}", dsnSrv);
            httpd_resp_sendstr(req, buf);
        }
    }
    else{
        httpd_resp_sendstr(req, "{\"message\" : \"DNS config was changed and saved\"}");
    }

    return ESP_OK;
}

/* post handlers */
esp_err_t post_set_config(httpd_req_t *req){
    char buf[256];
    uint8_t rOk;
    char *f[5];
    char *v[5];
    char *pwd = NULL;
    char *ssid = NULL;
    int k;
    int ok;
    int ret, remaining = req->content_len;

    while (remaining > 0){
        rOk = remaining < sizeof(buf) ? remaining : sizeof(buf);
        if ((ret = httpd_req_recv(req, buf, rOk)) <= 0){
            if (ret == HTTPD_SOCK_ERR_TIMEOUT){
                /* Retry receiving if timeout occurred */
                continue;
            }
            return ESP_FAIL;
        }
        if (remaining == sizeof(buf))
            buf[remaining - 1] = '\0';
        else
            buf[remaining] = '\0';
        remaining -= ret;
    }
    ok = parseRequest(buf, f, v, 5);
    for (k = 0; k < ok; k++){
        urlDecode(v[k]);
        if (strcmp("ssid", f[k]) == 0)
            ssid = v[k];
        else if (strcmp("pass", f[k]) == 0)
            pwd = v[k];
    }

    if (ssid && pwd){
        httpd_resp_send_chunk(req, "<b>Conectando...</b>", HTTPD_RESP_USE_STRLEN);
        wifi_setup_sta(ssid, pwd);
    }
    else{
        httpd_resp_send_chunk(req, "<b>Error en los parametros</b>", HTTPD_RESP_USE_STRLEN);
    }
    httpd_resp_send_chunk(req, NULL, 0);
    vTaskDelay(2000 / portTICK_PERIOD_MS);
    
    return ESP_OK;
}

esp_err_t update_handler(esp_http_client_event_t *evt){
    switch (evt->event_id) {
    case HTTP_EVENT_ERROR:

        break;
    case HTTP_EVENT_ON_CONNECTED:
        //httpd_resp_send_chunk((httpd_req_t *)evt->user_data, "<br /><br />Conectado.", HTTPD_RESP_USE_STRLEN);
        break;
    case HTTP_EVENT_HEADER_SENT:
        break;
    case HTTP_EVENT_ON_HEADER:
        break;
    case HTTP_EVENT_ON_DATA:
        //httpd_resp_send_chunk((httpd_req_t *)evt->user_data, ".", HTTPD_RESP_USE_STRLEN);
        break;
    case HTTP_EVENT_ON_FINISH:
        //httpd_resp_send_chunk((httpd_req_t *)evt->user_data, "<br /><br />Descarga finalizada.", HTTPD_RESP_USE_STRLEN);
        break;
    case HTTP_EVENT_DISCONNECTED:
        break;
    }
    return ESP_OK;
}

esp_err_t post_set_fwUpdate(httpd_req_t *req){
    esp_http_client_config_t config = {
        .url = FW_IMAGE_URL,
        .cert_pem = (char *)gitlab_cert_start,
        .event_handler = update_handler,
    };
    lamp_set_power(0);
    lamp_poweroff();
    cooler_set_power(0);
    cooler_poweroff();
    httpd_resp_send_chunk(req, "Iniciando actualizaci&oacute;n", HTTPD_RESP_USE_STRLEN);
    config.user_data = req;
    esp_err_t ret = esp_https_ota(&config);
    if (ret == ESP_OK) {
        httpd_resp_send_chunk(req, "<br /><br />Actualizaci&oacute;n finalizada exitosamente. Ahora se reiniciar&aacute; el sistema, espere por favor.", HTTPD_RESP_USE_STRLEN);
        httpd_resp_send_chunk(req, NULL, 0);
        vTaskDelay(500 / portTICK_PERIOD_MS);
        esp_restart();
    } else {
        httpd_resp_send_chunk(req, "<br /><br />La actualizaci&oacute;n fall&oacute;. Por favor, int&eacute;ntelo nuevamente m&aacute;s tarde.", HTTPD_RESP_USE_STRLEN);
        httpd_resp_send_chunk(req, NULL, 0);
        return ESP_FAIL;
    }
    return ESP_OK;
}

typedef struct{
    uint32_t delay;
    httpd_handle_t hd;
    int fd;
}streamParam;

void streamTask( void * pvParameters )
{
    httpd_ws_frame_t ws_pkt;
    uint32_t adc;
    float temp;
    char buf[128];
    memset(&ws_pkt, 0, sizeof(httpd_ws_frame_t));
    ws_pkt.payload = (uint8_t *)buf;
    ws_pkt.type = HTTPD_WS_TYPE_TEXT;
    ws_pkt.final = true;
    ws_pkt.fragmented = false;
    streamParam *ws = (streamParam *) pvParameters;
    while(1){
        adc = temp_get_value();
        temp = toCelsius(adc);
        ws_pkt.len = snprintf((char *)ws_pkt.payload, 
                              128, 
                              "{ \"status\":\"Ok\", \"param\":\"streamTemp\", \"value\": %f}", 
                              temp);
        httpd_ws_send_frame_async(ws->hd, ws->fd, &ws_pkt);
        vTaskDelay(ws->delay / portTICK_RATE_MS);
    }
}

/* WebSocket handler */
#define WSBUFFLEN 256
esp_err_t ws_handler(httpd_req_t *req){
    httpd_ws_frame_t ws_pkt;
    cJSON * json_str;
    cJSON * json_cmd;
    cJSON * json_value;
    static streamParam sp = {0, 0, 0};
    static  TaskHandle_t streamHandle = NULL;
    char cmdBuf[WSBUFFLEN];
    if (req->method == HTTP_GET) {
        return ESP_OK;
    }
    memset(&ws_pkt, 0, sizeof(httpd_ws_frame_t));
    ws_pkt.type = HTTPD_WS_TYPE_TEXT;
    /* Set max_len = 0 to get the frame len */
    ws_pkt.payload = (uint8_t *)cmdBuf;
    esp_err_t ret = httpd_ws_recv_frame(req, &ws_pkt, WSBUFFLEN);
    if (ret != ESP_OK) {
        return ret;
    }
    if ((ws_pkt.type == HTTPD_WS_TYPE_TEXT) && (ws_pkt.len > 0) && (ws_pkt.len < WSBUFFLEN)) {
        ws_pkt.payload[ws_pkt.len] = '\0';
        ws_pkt.final = true;
        ws_pkt.fragmented = false;
        json_str = cJSON_Parse((char *)ws_pkt.payload);
        if(json_str == NULL){
          ws_pkt.len = snprintf((char *)ws_pkt.payload, WSBUFFLEN, "{\"status\":\"Fail\", \"msg\":\"Failed to parse response\"}");
          return httpd_ws_send_frame(req, &ws_pkt);
        }
        json_cmd = cJSON_GetObjectItem(json_str, "cmd");
        json_value = cJSON_GetObjectItem(json_str, "parameter");
        if (!cJSON_IsNumber(json_value) || !cJSON_IsString(json_cmd)){
            cJSON_Delete(json_str);
            ws_pkt.len = snprintf((char *)ws_pkt.payload, WSBUFFLEN, "{\"status\":\"Fail\", \"msg\":\"Failed to parse response\"}");
            return httpd_ws_send_frame(req, &ws_pkt);
        }
        else{
            if(strcmp(json_cmd->valuestring, "getTemp") == 0){
                uint32_t adc = temp_get_value();
                float temp = toCelsius(adc);
                ws_pkt.len = snprintf((char *)ws_pkt.payload, 
                                      WSBUFFLEN, 
                                      "{ \"status\":\"Ok\", \"param\":\"temp\", \"value\": %f}"
                                      , temp);
            }
            else if(strcmp(json_cmd->valuestring, "getLamp") == 0){
                uint32_t pwr = lamp_get_power();
                ws_pkt.len = snprintf((char *)ws_pkt.payload, 
                                      WSBUFFLEN,  
                                      "{ \"status\":\"Ok\", \"param\":\"lamp\", \"value\": %u}", 
                                      pwr);
            }
            else if(strcmp(json_cmd->valuestring, "getCooler") == 0){
                uint32_t pwr = cooler_get_power();
                ws_pkt.len = snprintf((char *)ws_pkt.payload, 
                                      WSBUFFLEN, 
                                      "{ \"status\":\"Ok\", \"param\":\"cooler\", \"value\": %u}", 
                                      pwr);
            }
            else if(strcmp(json_cmd->valuestring, "setLamp") == 0){
                uint32_t pwr = json_value->valueint;
                lamp_set_power(pwr);
                ws_pkt.len = snprintf((char *)ws_pkt.payload, 
                                      WSBUFFLEN,  
                                      "{ \"status\":\"Ok\", \"param\":\"lamp\", \"value\": %u}", 
                                      pwr);
                
            }
            else if(strcmp(json_cmd->valuestring, "setCooler") == 0){
                uint32_t pwr = json_value->valueint;
                cooler_set_power(pwr);
                ws_pkt.len = snprintf((char *)ws_pkt.payload, 
                                      WSBUFFLEN, 
                                      "{ \"status\":\"Ok\", \"param\":\"cooler\", \"value\": %u}", 
                                      pwr);
            }
            else if(strcmp(json_cmd->valuestring, "startStream") == 0){
                if(sp.delay == 0){
                uint32_t dt = json_value->valueint;
                sp.delay = dt;
                sp.fd = httpd_req_to_sockfd(req);
                sp.hd = req->handle;
                ws_pkt.len = snprintf((char *)ws_pkt.payload, 
                                      WSBUFFLEN, 
                                      "{ \"status\":\"Ok\", \"param\":\"startStream\", \"value\": %u}", 
                                      dt);
                xTaskCreate(streamTask,
                    "streamTask",
                    2048, 
                    (void *)&sp,
                    tskIDLE_PRIORITY,
                    &streamHandle);
                }
                else{
                    ws_pkt.len = snprintf((char *)ws_pkt.payload, 
                                      WSBUFFLEN, 
                                      "{ \"status\":\"Fail\", \"param\":\"startStream\", \"value\": -1}");
                }
                
            }
            else if(strcmp(json_cmd->valuestring, "stopStream") == 0){
                if(streamHandle != NULL)
                    vTaskDelete( streamHandle );
                streamHandle = NULL;
                sp.delay = 0;
                ws_pkt.len = snprintf((char *)ws_pkt.payload, 
                                      WSBUFFLEN, 
                                      "{ \"status\":\"Ok\", \"param\":\"stopStream\", \"value\": 0}");
                
            }
            else{
                ws_pkt.len = snprintf((char *)ws_pkt.payload, WSBUFFLEN, "{\"status\":\"Fail\", \"msg\":\"Unknow arguments\"}");                                
            }
            cJSON_Delete(json_str);
            return httpd_ws_send_frame(req, &ws_pkt);
        }
    }
    else{
        ws_pkt.len = snprintf((char *)ws_pkt.payload, WSBUFFLEN, "{\"status\":\"Fail\", \"msg\":\"Bad request\"}");
        return httpd_ws_send_frame(req, &ws_pkt);
    }
}

/*
  URI difinitions for static pages
*/

httpd_uri_t uri_get_index = {
    .uri = "/",
    .method = HTTP_GET,
    .handler = get_index,
    .user_ctx = NULL};

httpd_uri_t uri_get_css_bootstrap = {
    .uri = "/css/bootstrap.css",
    .method = HTTP_GET,
    .handler = get_css_bootstrap,
    .user_ctx = NULL};

httpd_uri_t uri_get_css_grid = {
    .uri = "/css/grid.css",
    .method = HTTP_GET,
    .handler = get_css_grid,
    .user_ctx = NULL};

httpd_uri_t uri_get_css_reboot = {
    .uri = "/css/reboot.css",
    .method = HTTP_GET,
    .handler = get_css_reboot,
    .user_ctx = NULL};

httpd_uri_t uri_get_css_utilities = {
    .uri = "/css/utilities.css",
    .method = HTTP_GET,
    .handler = get_css_utilities,
    .user_ctx = NULL};

httpd_uri_t uri_get_img_logo = {
    .uri = "/img/logo.png",
    .method = HTTP_GET,
    .handler = get_img_logo,
    .user_ctx = NULL};

httpd_uri_t uri_get_js_bootstrap_boundle = {
    .uri = "/js/bootstrap.boundle.min.js",
    .method = HTTP_GET,
    .handler = get_js_bootstrap_boundle,
    .user_ctx = NULL};

httpd_uri_t uri_get_js_bootstrap_esm = {
    .uri = "/js/bootstrap.esm.min.js",
    .method = HTTP_GET,
    .handler = get_js_bootstrap_esm,
    .user_ctx = NULL};

httpd_uri_t uri_get_js_bootstrap = {
    .uri = "/js/bootstrap.min.js",
    .method = HTTP_GET,
    .handler = get_js_bootstrap_js,
    .user_ctx = NULL};

httpd_uri_t uri_get_js_jquery = {
    .uri = "/js/jquery-3.6.0.min.js",
    .method = HTTP_GET,
    .handler = get_js_jquery,
    .user_ctx = NULL};

httpd_uri_t uri_get_js_chart = {
    .uri = "/js/chart.min.js",
    .method = HTTP_GET,
    .handler = get_js_chart,
    .user_ctx = NULL};

httpd_uri_t uri_get_js_helpers = {
    .uri = "/js/helpers.esm.js",
    .method = HTTP_GET,
    .handler = get_js_helpers,
    .user_ctx = NULL};

httpd_uri_t uri_get_favicon = {
    .uri = "/favicon.jpg",
    .method = HTTP_GET,
    .handler = get_favicon,
    .user_ctx = NULL};

/*
  URI definitions for old getters
*/
httpd_uri_t uri_get_netList = {
    .uri = "/json/netList.json",
    .method = HTTP_GET,
    .handler = get_netList,
    .user_ctx = NULL};

httpd_uri_t uri_get_netStatus = {
    .uri = "/json/netStatus.json",
    .method = HTTP_GET,
    .handler = get_netStatus,
    .user_ctx = NULL};

httpd_uri_t uri_get_fwversion = {
    .uri = "/json/getFWVersion.json",
    .method = HTTP_GET,
    .handler = get_fwversion,
    .user_ctx = NULL};

httpd_uri_t uri_get_fwavail = {
    .uri = "/json/getFWAvailable.json",
    .method = HTTP_GET,
    .handler = get_fwavailable,
    .user_ctx = NULL};


/* API GET*/
httpd_uri_t uri_api_temp_get = {
    .uri = "/api/v1/temp",
    .method = HTTP_GET,
    .handler = api_temp_get,
    .user_ctx = NULL};

httpd_uri_t uri_api_lamp_get = {
    .uri = "/api/v1/lamp",
    .method = HTTP_GET,
    .handler = api_lamp_get,
    .user_ctx = NULL};

httpd_uri_t uri_api_lamp_set = {
    .uri = "/api/v1/lamp",
    .method = HTTP_POST,
    .handler = api_lamp_set,
    .user_ctx = NULL
};

httpd_uri_t uri_api_cooler_get = {
    .uri = "/api/v1/cooler",
    .method = HTTP_GET,
    .handler = api_cooler_get,
    .user_ctx = NULL};

httpd_uri_t uri_api_cooler_set = {
    .uri = "/api/v1/cooler",
    .method = HTTP_POST,
    .handler = api_cooler_set,
    .user_ctx = NULL};

httpd_uri_t uri_api_desc_get = {
    .uri = "/api/v1/desc",
    .method = HTTP_GET,
    .handler = api_desc_get,
    .user_ctx = NULL};

httpd_uri_t uri_api_desc_set = {
    .uri = "/api/v1/desc",
    .method = HTTP_POST,
    .handler = api_desc_set,
    .user_ctx = NULL};

httpd_uri_t uri_api_dns_get = {
    .uri = "/api/v1/dns",
    .method = HTTP_GET,
    .handler = api_dns_cfg_get,
    .user_ctx = NULL};

httpd_uri_t uri_api_dns_set = {
    .uri = "/api/v1/dns",
    .method = HTTP_POST,
    .handler = api_dns_cfg_set,
    .user_ctx = NULL};

/* post handlers */

httpd_uri_t uri_post_conf = {
    .uri = "/setConf.html",
    .method = HTTP_POST,
    .handler = post_set_config,
    .user_ctx = NULL
};

httpd_uri_t uri_post_fwUpdate = {
    .uri = "/doFwUpdate.html",
    .method = HTTP_POST,
    .handler = post_set_fwUpdate,
    .user_ctx = NULL
};

/* WebSocket URI*/
static const httpd_uri_t ws = {
        .uri        = "/ws",
        .method     = HTTP_GET,
        .handler    = ws_handler,
        .user_ctx   = NULL,
        .is_websocket = true
};


void setup_http(void)
{
    /* Generate default configuration */
    httpd_config_t config = HTTPD_DEFAULT_CONFIG();
    config.stack_size = 8192;
    config.max_uri_handlers = 40;
    config.max_open_sockets = 7;
    config.send_wait_timeout = 10;
    server = NULL;
    /* Start the httpd server */
    if (httpd_start(&server, &config) == ESP_OK)
    {
        /* Register URI handlers */
        httpd_register_uri_handler(server, &uri_get_index);
        httpd_register_uri_handler(server, &uri_get_css_bootstrap);
        httpd_register_uri_handler(server, &uri_get_css_grid);
        httpd_register_uri_handler(server, &uri_get_css_reboot);
        httpd_register_uri_handler(server, &uri_get_css_utilities);
        httpd_register_uri_handler(server, &uri_get_img_logo);
        httpd_register_uri_handler(server, &uri_get_js_bootstrap_boundle);
        httpd_register_uri_handler(server, &uri_get_js_bootstrap_esm);
        httpd_register_uri_handler(server, &uri_get_js_bootstrap);
        httpd_register_uri_handler(server, &uri_get_js_jquery);
        httpd_register_uri_handler(server, &uri_post_conf);
        httpd_register_uri_handler(server, &uri_get_netList);
        httpd_register_uri_handler(server, &uri_get_netStatus);
        httpd_register_uri_handler(server, &uri_get_js_chart);
        httpd_register_uri_handler(server, &uri_get_js_helpers);
        httpd_register_uri_handler(server, &uri_get_favicon);
        httpd_register_uri_handler(server, &uri_get_fwversion);
        httpd_register_uri_handler(server, &uri_post_fwUpdate);
        httpd_register_uri_handler(server, &uri_get_fwavail);
        /* API Register */
        httpd_register_uri_handler(server, &uri_api_temp_get);
        httpd_register_uri_handler(server, &uri_api_lamp_get);
        httpd_register_uri_handler(server, &uri_api_lamp_set);
        httpd_register_uri_handler(server, &uri_api_cooler_get);
        httpd_register_uri_handler(server, &uri_api_cooler_set);
        httpd_register_uri_handler(server, &uri_api_desc_get);
        httpd_register_uri_handler(server, &uri_api_desc_set);
        httpd_register_uri_handler(server, &uri_api_dns_set);
        httpd_register_uri_handler(server, &uri_api_dns_get);
        /* WebSocket */
        httpd_register_uri_handler(server, &ws);
    }
}

/* Function for stopping the webserver */
void stop_webserver(void)
{
    if (server)
    {
        /* Stop the httpd server */
        httpd_stop(server);
    }
}