#ifndef serial_com
#define serial_com

#include <stdint.h>

/*
Protocolo serie utilizado:
- El protocolo utiliza header (0x0C)
- 1 byte de comando
- 5 de pyload
- 1 byte de CRC en 8 bits.
*/
#define dataLen 5
#define pkgLen  8

typedef struct {
    uint8_t header;
    uint8_t cmd;
    uint8_t data[dataLen];
    uint8_t crc;
} __attribute__((packed))  tserial_pkg;

typedef union {
  tserial_pkg pkg;
  uint8_t bytes[pkgLen];
} __attribute__((packed))  serial_pkg ;



/*
 Command list:
*/

#define GET_TEMP         0x01
#define LAMP_POWER_OFF   0x02
#define LAMP_SET_POWER   0x03
#define COOLER_POWER_OFF 0x04
#define COOLER_SET_POWER 0x05


#define RTD_A 3.9083e-3
#define RTD_B -5.775e-7

void serial_init(void);
void lamp_set_power(uint8_t pwd);
uint8_t lamp_get_power(void);
void lamp_poweroff(void);
void cooler_set_power(uint8_t pwd);
uint8_t cooler_get_power(void);
void cooler_poweroff(void);
uint16_t temp_get_value(void);

double toCelsius(uint16_t adc);
#endif