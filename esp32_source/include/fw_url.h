#ifndef FW_URL
#define FW_URL

#define FW_INFO_HOST "gitlab.com"
#define FW_INFO_PATH "/labinformatica/tempcontrol/-/raw/main/release/fw_info.json"

#define FW_IMAGE_URL "https://gitlab.com/labinformatica/tempcontrol/-/raw/main/release/firmware.bin"
#endif