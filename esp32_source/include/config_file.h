#ifndef config_file_h
#define config_file_h
#include <esp_http_server.h>
#define MAX_DESC_LEN 256
#define MAX_DNS_CERT 4000
typedef struct{
  char desc[MAX_DESC_LEN];
  char dns_update_uri[HTTPD_MAX_URI_LEN];
  bool dns_update_enable;
  char dns_cert[MAX_DNS_CERT];
} config_data;

config_data * get_config_data(void);
void load_config_data(void);
void save_config_data(void);

#endif