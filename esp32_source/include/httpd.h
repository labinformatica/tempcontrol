#ifndef httpd_h
#define httpd_h
#include <esp_http_server.h>

uint8_t hex2dec(char c);
void json_esc_str(char *, int);
int parseRequest(char *str, char *field[], char *values[], int len);
void urlDecode(char *str);
/* funciones asociadas a las diferentes uris */
esp_err_t get_index(httpd_req_t *req);
esp_err_t get_css_bootstrap(httpd_req_t *req);
esp_err_t get_css_grid(httpd_req_t *req);
esp_err_t get_css_reboot(httpd_req_t *req);
esp_err_t get_css_utilities(httpd_req_t *req);
esp_err_t get_img_logo(httpd_req_t *req);
esp_err_t get_js_bootstrap_boundle(httpd_req_t *req);
esp_err_t get_js_bootstrap_esm(httpd_req_t *req);
esp_err_t get_js_bootstrap_js(httpd_req_t *req);
esp_err_t get_js_jquery(httpd_req_t *req);
esp_err_t get_js_chart(httpd_req_t *req);
esp_err_t get_js_helpers(httpd_req_t *req);
esp_err_t get_favicon(httpd_req_t *req);

/* json getters */
esp_err_t get_netList(httpd_req_t *req);
esp_err_t get_netStatus(httpd_req_t *req);
esp_err_t get_fwversion(httpd_req_t *req);
esp_err_t get_fwavailable(httpd_req_t *req);

/* API GET*/
esp_err_t api_get_getTemp(httpd_req_t *req);
esp_err_t api_get_getLamp(httpd_req_t *req);
esp_err_t api_get_getCooler(httpd_req_t *req);
esp_err_t api_get_getDesc(httpd_req_t *req);

/* post handlers */
esp_err_t post_set_config(httpd_req_t *req);
esp_err_t post_set_fwUpdate(httpd_req_t *req);

/* API POST*/
esp_err_t api_post_setLamp(httpd_req_t *req);
esp_err_t api_post_setCooler(httpd_req_t *req);
esp_err_t api_post_setDesc(httpd_req_t *req);

/* WebSocket handler */

esp_err_t ws_handler(httpd_req_t *req);

/* fin */

void setup_http(void);
void stop_webserver(void);
#endif 