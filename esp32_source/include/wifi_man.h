#ifndef wifi_man
#define wifi_man

#include "esp_system.h"
#include "esp_wifi.h"

#define MAX_SCAN_NETS 15

void wifi_setup_ap(void);
void wifi_setup_sta(char *ssid, char * pwd);
void wifi_scan(uint16_t *detectedCnt, wifi_ap_record_t *ap_info[]);
void wifi_sta_info(esp_netif_ip_info_t *ip_info);

#endif