#ifndef dns_reg_h
#define dns_reg_h
#include <stdint.h>

typedef struct {
  char *data;
  int data_len;

} thttp_user_data;

uint8_t update_dns_reg(char *ret, int len);

#endif
