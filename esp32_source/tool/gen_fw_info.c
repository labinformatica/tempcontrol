#include <stdio.h>
#include "../include/version.h"

int main(int argc, char *argv[]){
    FILE * fw_info = fopen("fw_info.json", "wt");
    fprintf(fw_info, "{\"fwVer\": \"%s\",\"fwDate\": \"%s\",\"fwTime\": \"%s\"}", fwver, build_date, build_time);
    fclose(fw_info);
    return 0; 
}