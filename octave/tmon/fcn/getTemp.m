%-*- texinfo -*-
%@deftypefn {Function File} {@var{ret} =}  getTemp()
%Function getTemp()
%@end deftypefn

function ret =  getTemp (addr)
  [s, ok, msg] = urlread([addr "/api/v1/temp"], "get", {"",""});
  if (ok)
    obj = loadjson(s);
    ret = obj.temp;
  else
    ret = 0;
  endif
endfunction
