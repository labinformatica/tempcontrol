%-*- texinfo -*-
%@deftypefn {Function File} {@var{ret} =}  setCooler()
%Function setCooler()
%@end deftypefn

function ret = setCooler (addr, power)
  [s, ok, msg] = urlread([addr "/api/v1/cooler"], "post", {"json",["{\"power\":" num2str(power) "}"]});
  ret = ok;
endfunction
