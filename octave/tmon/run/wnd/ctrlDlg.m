## -*- texinfo -*-
## @deftypefn  {} {@var{wnd} =} ctrlDlg ()
##
## Create and show the dialog, return a struct as representation of dialog.
##
## @end deftypefn
function wnd = ctrlDlg(varargin)
  ctrlDlg_def;
  wnd = show_ctrlDlg(varargin{:});
end
