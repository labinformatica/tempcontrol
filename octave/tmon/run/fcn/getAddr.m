%-*- texinfo -*-
%@deftypefn {Function File} {@var{ret} =}  getAddr()
%Function getAddr()
%@end deftypefn

function ret =  getAddr (force = false)
  global _labAddr;
  if (force)
    _labAddr = "";
  endif
  if (length(_labAddr) == 0)
    hwnd = ipDlg();
    waitfor(hwnd.figure);
  endif
  ret = _labAddr;
endfunction
