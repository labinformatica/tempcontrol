%-*- texinfo -*-
%@deftypefn {Function File} {@var{ret} =}  setLamp()
%Function setLamp()
%@end deftypefn

function ret = setLamp (addr, power)
  [s, ok, msg] = urlread([addr "/api/v1/lamp"], "post", {"json",["{\"power\":" num2str(power) "}"]});
  ret = ok;
endfunction
