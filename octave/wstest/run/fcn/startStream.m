%-*- texinfo -*-
%@deftypefn {Function File} {@var{ret} =}  startStream()
%Function startStream()
%@end deftypefn

function  startStream (ws, ts)
  ws_send (ws, ["{\"cmd\":\"startStream\", \"parameter\":" num2str(ts) "}"], 'text', 1);  
  newVals = ws_receive (ws);
  resp = loadjson(newVals);
  if(strcmp(resp.status,"Ok") != 1)
    disp('Error starting data stream');
  endif
endfunction
