%-*- texinfo -*-
%@deftypefn {Function File} {@var{ret} =}  setCooler()
%Function setCooler()
%@end deftypefn

function ret = setCooler (ws, power)
  ws_send (ws, ["{\"cmd\":\"setCooler\", \"parameter\":" num2str(power) "}"], 'text', 1); 
  newVals = ws_receive (ws);
  resp = loadjson(newVals);
  if(strcmp(resp.status, "Ok") != 1)
    disp('Error setting cooler power');
    ret = false;
  else
    ret = true;
  endif
endfunction