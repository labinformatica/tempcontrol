%-*- texinfo -*-
%@deftypefn {Function File} {@var{ret} =}  getTemp()
%Function getTemp()
%@end deftypefn

function [ok, val] =  getTemp (ws)
  ws_send (ws, ["{\"cmd\":\"getTemp\", \"parameter\":0}"], 'text', 1); 
  newVals = ws_receive (ws);
  resp = loadjson(newVals);
  if((strcmp(resp.status,"Ok") == 1) && (strcmp(resp.param, "temp")==1))
    ok = true;
    val = resp.value;
  else
    ok = false;
    val = 0;
  endif
endfunction
