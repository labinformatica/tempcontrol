%-*- texinfo -*-
%@deftypefn {Function File} {@var{ret} =}  stopStream()
%Function stopStream()
%@end deftypefn

function stopStream (ws)
  ws_send (ws, 
           ["{\"cmd\":\"stopStream\", \"parameter\":" num2str(0) "}"], 
           'text', 1);
endfunction
