## -*- texinfo -*-
## @deftypefn  {} {@var{wnd} =} ipDlg ()
##
## Create and show the dialog, return a struct as representation of dialog.
##
## @end deftypefn
function wnd = ipDlg(varargin)
  ipDlg_def;
  wnd = show_ipDlg(varargin{:});
end
