function ret = runApp(varargin)
  [dir, name, ext] = fileparts( mfilename('fullpathext') );
  global _wstestBasePath = dir;
  global _wstestImgPath = [dir filesep() 'img'];
  addpath([dir filesep() "libs" ]);
  addpath([dir filesep() "fcn" ]);
  addpath([dir filesep() "wnd" ]);
  addpath([dir filesep() "guilib" ]);
  waitfor(mainDlg().figure);
end
