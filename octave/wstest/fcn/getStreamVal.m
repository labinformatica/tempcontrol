%-*- texinfo -*-
%@deftypefn {Function File} {@var{ret} =}  getStreamVal()
%Function getStreamVal()
%@end deftypefn

function [ok, val] =  getStreamVal (ws)
  newVals = ws_receive (ws);
  resp = loadjson(newVals);
  resp
  if((strcmp(resp.status, "Ok")==1) && (strcmp(resp.param,"streamTemp")==1))
    ok = true;
    val = resp.value;
  else
    ok = false;
    val = 0;
  endif
endfunction
