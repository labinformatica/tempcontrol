%-*- texinfo -*-
%@deftypefn {Function File} {@var{ret} =}  setLamp()
%Function setLamp()
%@end deftypefn

function ret = setLamp (ws, power)
  ws_send (ws, ["{\"cmd\":\"setLamp\", \"parameter\":" num2str(power) "}"], 'text', 1); 
  newVals = ws_receive (ws);
  resp = loadjson(newVals);
  if (strcmp(resp.status, "Ok")!= 1)
    disp('Error setting lamp power');
    ret = false;
  else
    ret = true;
  endif
endfunction
