# tempcontrol

This project is about a simple control thermal plant including a lamp (as heater), a temperature sensor (pt100 sensor with a max31865 module) and a cooler as disturbances generator.

<div align="center">
<img src="/3dmodel/render_01.png" width="250" />
</div>

The system has been designed in order to allow sensing temperature, control the power of the heating lamp and generate disturbances using a cooler whose speed can also be controlled.
The goal of the system is to allow process control experiments to be carried out using GNU Octave with applications developed in guiEditor.

## Repository Structure

Here you will find all the necessary information in order to reproduce the system, both hardware and software. In order to facilitate its construction, commercial electronic modules were used for prototyping.
The detail of the repository content is:

- 3d models: the structure has been designed using 3d printing and is made up of 3 platforms joined together using threaded rods. The models are in the [3dmodel](/3dmodel) folder.

- Electronic circuit: interconnect schematic is included in [/circuit/design](/circuit/design).

- ESP-32 software: the communications center of the system is given by this module. It is designed in order to have a rest api that makes it possible to control the system. It also supports communication based on web sockets in order to improve the response speed of the temperature sensor. The development has been done using VSCode, Platform.IO and C language. ([esp32_source](/esp32_source))

- STM32F103 software: peripheral control tasks are performed by this module. In the folder [stm32f103_fw](/stm32f103_fw) is the firmware developed using STM32 CubeIDE.

- GNU Octave Applications: Two communications test applications are available in the [octave](/octave) folder. Both allow monitoring and control of the system, the difference is that in one web sockets are used while in the other the api rest to access the information. Both applications were developed using [guiEditor](https://gitlab.com/labinformatica/guieditor) in order to improve the user experience.

Finally, in the release folder, the latest version of the firmware for the ESP32 module compiled in the cloud is stored. This file is used in order to update system software via OTA.
