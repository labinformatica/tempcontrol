/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define led_Pin GPIO_PIN_13
#define led_GPIO_Port GPIOC
#define ZCD_Pin GPIO_PIN_10
#define ZCD_GPIO_Port GPIOB
#define ZCD_EXTI_IRQn EXTI15_10_IRQn
#define trigger_Pin GPIO_PIN_11
#define trigger_GPIO_Port GPIOB
#define SPI2_CS_Pin GPIO_PIN_12
#define SPI2_CS_GPIO_Port GPIOB
#define MaxRdy_Pin GPIO_PIN_9
#define MaxRdy_GPIO_Port GPIOA
#define MaxRdy_EXTI_IRQn EXTI9_5_IRQn
/* USER CODE BEGIN Private defines */

#define MAX_VIAS_ON 0x80
#define MAX_AUTO 0x40
#define MAX_1SHOT 0x20
#define MAX_3WIRE 0x10
#define MAX_FD3 0x08
#define MAX_FD2 0x04
#define MAX_CLEAR_FAULT 0x02
#define MAX_50HZ_F 0x01

#define RTD_A 3.9083e-3
#define RTD_B -5.775e-7

/*
Protocolo serie utilizado:
- El protocolo utiliza header (0x0C)
- 1 byte de comando
- 5 de pyload
- 1 byte de CRC en 8 bits.
*/
#define dataLen 5
#define pkgLen  8

typedef struct {
    uint8_t header;
    uint8_t cmd;
    uint8_t data[dataLen];
    uint8_t crc;
} __attribute__((packed))  tserial_pkg;

typedef union {
  tserial_pkg pkg;
  uint8_t bytes[pkgLen];
} __attribute__((packed))  serial_pkg ;

typedef struct {
	serial_pkg rcv_data;
	uint8_t data_cnt;
	uint8_t state;
} serial_pkg_states;

/*
 Command list:
*/

#define GET_TEMP         0x01
#define LAMP_POWER_OFF   0x02
#define LAMP_SET_POWER   0x03
#define COOLER_POWER_OFF 0x04
#define COOLER_SET_POWER 0x05
#define MINCMD GET_TEMP
#define MAXCMD COOLER_SET_POWER

typedef uint8_t (*fcn_state)(serial_pkg_states *, uint8_t);

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
